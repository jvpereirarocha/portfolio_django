# Generated by Django 3.0.1 on 2019-12-30 16:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20191230_1143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projetos',
            name='imagem',
            field=models.ImageField(max_length=200, upload_to='projetos'),
        ),
    ]
