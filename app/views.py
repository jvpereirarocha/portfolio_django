from django.shortcuts import render, redirect
from .models import Projetos, SobreMim, Certificados, Formacoes,\
    Contato, ExperienciaProfissional
from django.views.decorators.http import require_http_methods
from django.core.paginator import Paginator


def index(request):
    projetos = Projetos.objects.all()
    sobre_mim = SobreMim.objects.first()
    certificados = Certificados.objects.all()
    formacoes = Formacoes.objects.all()
    experiencias = ExperienciaProfissional.objects.all()
    count = len(projetos)
    paginator = Paginator(projetos, 15)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'projetos': projetos,
        'sobre_mim': sobre_mim,
        'certificados': certificados,
        'formacoes': formacoes,
        'qtd': count,
        'page_obj': page_obj,
        'experiencias': experiencias,
    }
    return render(request, 'index.html', context)


@require_http_methods(["POST"])
def enviar(request):
    if request.method == 'POST':
        nome = request.POST.get('nome', None)
        telefone = request.POST.get('telefone', None)
        email = request.POST.get('email', None)
        mensagem = request.POST.get('mensagem', None)

        if nome is not None and telefone is not None\
                and email is not None and mensagem is not None:
            contato = Contato(nome=nome, telefone=telefone, email=email,
                              mensagem=mensagem)
            contato.save()
            return redirect('portfolio:home')

    return render(request, 'index.html')
