from django.db import models
from django.utils.translation import gettext_lazy as _


class Tecnologias(models.Model):

    class TiposTecnologia(models.TextChoices):
        BACKEND = 'Back', _('Back-End')
        FRONTEND = 'Front', _('Front-End')
        INFRA = 'Infra', _('Infraestrutura')
        VERSIONAMENTO = 'Versionamento', _('Versionamento')
        FERRAMENTA = 'Ferramenta', _('Ferramenta')
        BANCO_DADOS = 'Banco_dados', _('Banco de Dados')

    nome = models.CharField(max_length=100, verbose_name='Tecnologia')
    tipo_tecnologia = models.CharField(
        max_length=30,
        choices=TiposTecnologia.choices,
        default=TiposTecnologia.BACKEND,
    )
    is_framework = models.BooleanField(default=False,
                                       verbose_name='É Framework')

    def __str__(self):
        return "{nome}".format(nome=self.nome)

    class Meta:
        verbose_name = 'Tecnologia'
        verbose_name_plural = 'Tecnologias'

    def tipo(self):
        return self.tipo_tecnologia

    tipo.admin_order_field = 'tipo_tecnologia'


class Projetos(models.Model):

    def default_url():
        return 'https://www.google.com/'

    titulo = models.CharField(max_length=100, verbose_name='Titulo')
    descricao = models.TextField()
    imagem = models.ImageField(upload_to='projetos', max_length=200,
                               help_text='A altura recomendada para a imagem é 150px')
    link = models.URLField(max_length=256, default=default_url)
    tecnologias = models.ManyToManyField(Tecnologias, related_name='tecnologias')

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = 'Projeto'
        verbose_name_plural = 'Projetos'
        ordering = ['titulo']


class Certificados(models.Model):
    descricao = models.CharField(max_length=256, help_text='Ex.: Curso online...')
    instituicao = models.CharField(max_length=256, help_text='Ex.: Udemy Cursos')

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Certificado'
        verbose_name_plural = 'Certificados'
        ordering = ['descricao']


class Formacoes(models.Model):
    curso = models.CharField(max_length=100)
    grau = models.CharField(max_length=100)
    instituicao = models.CharField(max_length=100)
    data_conclusao = models.DateField()

    def __str__(self):
        return self.curso

    class Meta:
        verbose_name = 'Formação'
        verbose_name_plural = 'Formações'
        ordering = ['-data_conclusao']


class SobreMim(models.Model):

    resumo = models.TextField(max_length=30, help_text='Breve resumo')
    descricao_completa = models.TextField(max_length=2048, help_text='Máximo de 2048 caracteres')
    certificados = models.ManyToManyField(Certificados, related_name='certificados')
    formacoes = models.ManyToManyField(Formacoes, related_name='formacoes')

    def __str__(self):
        return self.resumo

    class Meta:
        verbose_name = 'Sobre Mim'
        verbose_name_plural = 'Sobre Mim'


class Contato(models.Model):
    nome = models.CharField(max_length=100)
    telefone = models.CharField(max_length=20)
    email = models.EmailField()
    mensagem = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{nome} - {email}".format(nome=self.nome, email=self.email)

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'


class ExperienciaProfissional(models.Model):
    cargo = models.CharField(max_length=100, verbose_name='Cargo')
    empresa = models.CharField(max_length=255)
    atividades = models.TextField(max_length=2000)
    data_inicial = models.DateField()
    data_final = models.DateField(blank=True, null=True)
    atualmente = models.BooleanField(default=False)

    def __str__(self):
        return "{emp} - {dt}".format(emp=self.empresa, dt=self.data_inicial)

    class Meta:
        verbose_name = 'Experiência Profissional'
        verbose_name_plural = 'Experiências Profissionais'
