from django.contrib import admin
from .models import Projetos, Tecnologias, SobreMim, Certificados, Formacoes, Contato,\
    ExperienciaProfissional


class TecnologiasAdmin(admin.ModelAdmin):
    list_display = ('nome', 'tipo')
    ordering = ['tipo_tecnologia']


admin.site.register(Projetos)
admin.site.register(Tecnologias, TecnologiasAdmin)
admin.site.register(SobreMim)
admin.site.register(Certificados)
admin.site.register(Formacoes)
admin.site.register(Contato)
admin.site.register(ExperienciaProfissional)